import { Component } from '@angular/core';

@Component({
  selector: 'app-perfilangel',
  templateUrl: './perfilangel.component.html',
  styleUrls: ['./perfilangel.component.css']
})

export class PerfilangelComponent {
  name = 'Guzmán Gómez Angel Hiramg';
  ocupacion = 'Estudiante del Tecnológico de Cuautla';

  strengths = [{ text: 'Nivel de ingles', level: 'Avanzado' }, { text: 'Nivel de HTML', level: 'Bueno' }, { text: 'Nivel de Angular', level: 'Aprendiz'}, {text: 'Nivel de React', level:'Malo' }, {text: 'Nivel de creatividad', level:'Buena' }];

}
