import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilangelComponent } from './perfilangel.component';

describe('PerfilangelComponent', () => {
  let component: PerfilangelComponent;
  let fixture: ComponentFixture<PerfilangelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilangelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PerfilangelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
